import {Appmenu} from "./Appmenu.js";

export class Taskbar {
    constructor(desktop) {
        this.desktop = desktop;
        this.appmenu = new Appmenu(desktop, this);
    }

    spawnTaskbar() {
        let self = this;
        let taskbar;
        {
            let appButton;
            {
                appButton = document.createElement("div");
                appButton.id = "taskbar_appButton";
                appButton.innerHTML = "Applications";
            }
            taskbar = this.taskbar = document.createElement("div");
            taskbar.id = "taskbar_main";
            taskbar.appendChild(appButton);
            this.appmenu.spawnMenu(appButton, taskbar, desktop.main);
        }
        return taskbar;
    }


}
