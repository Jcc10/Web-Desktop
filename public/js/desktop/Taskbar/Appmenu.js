import {Settings} from "./../Apps/System/Settings.js";

export class Appmenu {
    constructor(desktop, taskbar) {
        this.desktop = desktop;
        this.taskbar = taskbar;
        this.visible;
        this.settings = new Settings(this.desktop);
    }

    spawnMenu(appButton) {
        this.appButton = appButton;
        let menu;
        let self = this;
        menu = this.menu = document.createElement("div");
        menu.id = "appmenu_main";
        {
            let section;
            {
                section = document.createElement("div");
                section.id = "appmenu_appList";
                menu.appendChild(section)
            }
            {
                section = document.createElement("div");
                section.id = "appmenu_catagoryList";
                menu.appendChild(section)
            }
            {
                section = document.createElement("div");
                section.id = "appmenu_controlButtons";
                {
                    let settings = document.createElement("div");
                    settings.id = "appmenu_settingsButton";
                    settings.innerHTML = "S";
                    settings.title = "Settings";
                    settings.addEventListener("click", () => {self.settings.openWindow()})
                    section.appendChild(settings);
                }
                {
                    let logout = document.createElement("div");
                    logout.id = "appmenu_logoutButton";
                    logout.innerHTML = "X";
                    logout.title = "Logout";
                    logout.addEventListener("click", () => {self.desktop.logout()})
                    section.appendChild(logout);
                }
                menu.appendChild(section)
            }
            {
                section = document.createElement("div");
                section.id = "appmenu_userSpace";
                menu.appendChild(section)
            }
        }
        this.taskbar.taskbar.appendChild(menu);
        this.hideMenu(null);
        appButton.addEventListener("click", ()=>{self.toggleMenu()});
        this.desktop.main.addEventListener("click", (e)=>{self.__hideMenu(e)});
    }

    toggleMenu() {
        if(this.visible == true){
            this.menu.style.visibility = "hidden";
            this.visible = false;
        } else {
            this.menu.style.visibility = "visible";
            this.visible = true;
        }
    }

    showMenu() {
        this.menu.style.visibility = "visible";
        this.visible = true;
    }


    hideMenu() {
        this.menu.style.visibility = "hidden";
        this.visible = false;
    }

    __hideMenu(e) {
        if(this.visible == true){
            if(!this.menu.contains(e.target) && !this.appButton.contains(e.target)){
                this.menu.style.visibility = "hidden";
            }
        }
    }
}
