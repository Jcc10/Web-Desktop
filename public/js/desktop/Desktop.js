import {sleep} from "./../sleep.polyfill.js";
import {Taskbar} from "./Taskbar/Taskbar.js";

export class Desktop {
    system;
    login;
    taskbar;
    constructor(system, login) {
        this.system = system;
        this.login = login;
        this.taskbar = new Taskbar(this);
    }

    async spawnDesktop() {
        console.log("Desktop Should Be Spawned Here");
        let main = this.main = document.createElement("div");
        let taskbar = this.taskbar.spawnTaskbar();
        {
            let windowSpace = this.windowSpace = document.createElement("div");
            windowSpace.id = "desktop_appwindowspace";
            main.appendChild(windowSpace);
            main.id = "desktop_main";
            main.appendChild(taskbar);
        }

        let wrapper = this.system.getMain();
        wrapper.prepend(main);

        // System is loaded.
        this.system.setStatus("Welcome!");
        await this.system.stopLoad();
        this.system.setStatus("Ready.");
    }

    destroyDesktop() {
        let self = this;
        return new Promise((resolve, reject) => {
            self.main.parentNode.removeChild(self.main);
            resolve();
        });
    }

    logout() {
        this.login.logout();
    }
}
