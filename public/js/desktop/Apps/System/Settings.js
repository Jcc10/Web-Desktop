import {TestWindow} from "./../../Application/TestWindow.js";

export class Settings {
    constructor(desktop) {
        this.desktop = desktop;
    }

    openWindow() {
        let testWindow = new TestWindow();
        testWindow.addWindow(this.desktop);
    }
}
