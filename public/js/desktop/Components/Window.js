export class Window {
    titlebar;
    window;

    constructor() {
        // titlebar
        {
            this.titlebar = document.createElement("div");
            this.titlebar.addEventListener("mousedown", (e)=>this.__mouseDown(e));
            this.titlebar.classList.add("titlebar");
        }
        this.window = document.createElement("div");
        this.window.appendChild(this.titlebar);
        this.window.classList.add("window");
        this.window.classList.add("minimised");
    }

    addWindow(desktop) {
        desktop.windowSpace.prepend(this.window);
        this.window.classList.remove("minimised");
    }

    __mouseDown(e) {
        // get the mouse cursor position at startup:
        let origin = [e.clientX, e.clientY];
        let dest = [0, 0];
        let closeDrag, moveDrag;
        closeDrag = () => {
            // stop moving when mouse button is released:
            document.removeEventListener("mouseup", closeDrag);
            document.removeEventListener("mouseleave", closeDrag);
            document.removeEventListener("mousemove", moveDrag);
        };

        moveDrag = (e) => {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            dest[0] = origin[0] - e.clientX;
            dest[1] = origin[1] - e.clientY;
            origin[0] = e.clientX;
            origin[1] = e.clientY;
            // set the element's new position:
            this.window.style.top = (this.window.offsetTop - dest[1]) + "px";
            this.window.style.left = (this.window.offsetLeft - dest[0]) + "px";
        }

        e.preventDefault();

        document.addEventListener("mouseup", closeDrag);
        document.addEventListener("mouseleave", closeDrag);
        document.addEventListener("mousemove", moveDrag);
    }
}
