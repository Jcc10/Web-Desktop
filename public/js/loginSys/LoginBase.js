import {Desktop} from './../desktop/Desktop.js';
export class LoginBase {
    system;
    desktop = null;
    loginMain = null;

    passwordBox;
    usernameBox;

    constructor(system) {
        this.system = system;
        this.__createLoginBox();
    }

    spawnLogin() {
        if(this.loginMain == null){
            this.__createLoginBox();
        }
    }

    __createLoginBox() {
        let self = this;
        let loginBox
        {
            let passwordBox, usernameBox, loginButton;
            // content of the loginBox
            {
                // passwordBox (do password before login so login can focus on password)
                {
                    passwordBox = this.passwordBox = document.createElement("input");
                    passwordBox.id = "login_password";
                    passwordBox.type = "password";
                    passwordBox.placeholder = "Password";
                    passwordBox.addEventListener("keypress", (e) => {
                        var key = e.which || e.keyCode;
                        if (key === 13) { // 13 is enter
                            self.login();
                        }
                    });
                }
                // usernameBox
                {
                    usernameBox = this.usernameBox = document.createElement("input");
                    usernameBox.id = "login_username";
                    usernameBox.type = "text";
                    usernameBox.placeholder = "Username";
                    usernameBox.addEventListener("keypress", (e) => {
                        var key = e.which || e.keyCode;
                        if (key === 13) { // 13 is enter
                            passwordBox.focus();
                        }
                    });
                }
                //loginButton
                {
                    loginButton = document.createElement("button");
                    loginButton.type = "button";
                    loginButton.id = loginButton.name = "login_loginButton";
                    loginButton.innerHTML = "X";
                    loginButton.title = "Login";
                    loginButton.addEventListener("click", () => {self.login();});
                    loginButton.addEventListener("keypress", (e) => {
                        var key = e.which || e.keyCode;
                        if (key === 13) { // 13 is enter
                            self.login();
                        }
                    });
                }
            }
            // loginBox itself
            {
                loginBox = document.createElement("div");
                loginBox.id = "login_box";
                loginBox.appendChild(usernameBox);
                loginBox.appendChild(document.createElement("br"));
                loginBox.appendChild(passwordBox);
                loginBox.appendChild(document.createElement("br"));
                loginBox.appendChild(loginButton);
            }

        }

        let loginMain = this.loginMain = document.createElement("div");
        {
            loginMain.id = "login_main";
            loginMain.appendChild(loginBox);
        }

        let main = this.system.getMain();
        main.prepend(loginMain);
    }

    destroyLogin() {
        this.loginMain.parentNode.removeChild(this.loginMain);
        this.loginMain = null;
    }

    async login() {
        this.system.setStatus("Logging in...");
        let loadPromise = this.system.startLoad();
        this.desktop = window.desktop = new Desktop(this.system, this);
        await loadPromise;
        this.passwordBox.value = "";
        this.destroyLogin();
        this.desktop.spawnDesktop();
    }

    async logout() {
        this.system.setStatus("Logging out...");
        await this.system.startLoad();
        await this.desktop.destroyDesktop();
        this.desktop = window.desktop = null;
        this.spawnLogin();
        this.system.stopLoad();
        this.system.setStatus("Done");
    }
}
