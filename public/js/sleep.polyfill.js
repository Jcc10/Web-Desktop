export function sleep(timeout) {
    return new Promise(function(resolve, reject) {
        setTimeout(resolve, timeout);
    });
}

/**
 * Sleep polyfill, returns promise that resolves when sleep is done.
 * Used for async functions.
 */
