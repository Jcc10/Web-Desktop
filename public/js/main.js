/* jshint esversion:6 */
import {sleep} from './sleep.polyfill.js'; // Sleep polyfill.
import {System} from './System.js';
import {LoginBase} from './loginSys/LoginBase.js';


/* this will run when the page is fully loaded. */
async function onLoadScript() {
    let system = window.system = new System(
                                document.getElementById('system_main'),
                                document.getElementById('system_status'),
                                document.getElementById('system_loadingMain')
                            );
    let loginSys = window.login = new LoginBase(system);
    await sleep(500);
    await system.stopLoad();
    system.setStatus("Done.", "Done loading");
}


// run the `onLoadScript` when the page is loaded.
window.onload = onLoadScript;
