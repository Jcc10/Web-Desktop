import {sleep} from "./sleep.polyfill.js";

// This is more for human readability, the numbers are what are used in-code.
export const loadingModes = {
    "NotLoading": 0,
    "Loading": 1,
    "LoadOut": 2,
    "LoadIn": 3
};

export class System {
    __main;
    __status;
    __loader;
    __loadStatus = loadingModes.Loading;
    __loadOutPromise;
    __loadInPromise;
    __loaderOpacity = 1;

    constructor(mainElement, statusElement, fullPageLoader) {
        this.__main = mainElement;
        this.__status = statusElement;
        this.__loader = fullPageLoader;
    }

    getMain() {
        return this.__main;
    }

    setStatus(message) {
        if(this.__status){
            this.setStatus(message, "");
        }
    }

    setStatus(message, hint) {
        if(this.__status){
            this.__status.innerHTML = message;
            this.__status.title = hint;
        }
    }

    async startLoad() {
        switch (this.__loadStatus) {
            case loadingModes.NotLoading:
            case loadingModes.LoadOut:
                this.__loadStatus = loadingModes.LoadIn;
                this.__loaderOpacity = 0;
                this.__loader.style.visibility = "visible";
                this.__loadInPromise = this.__startLoadAnimation();
                await this.__loadInPromise;
                break;
            case loadingModes.Loading:
                break;
            case loadingModes.LoadIn:
                await this.__loadInPromise;
                break;
        }
        return true;
    }

    async __startLoadAnimation() {
        while((this.__loaderOpacity < 1) && (this.__loadStatus == loadingModes.LoadIn)){
            this.__loaderOpacity += 0.01;
            this.__loader.style.opacity = this.__loaderOpacity;
            await sleep(10);
        }
        this.__loadStatus == loadingModes.Loading;
        this.__loaderOpacity = 1;
        this.__loader.style.opacity = this.__loaderOpacity;
        return true;
    }

    async stopLoad() {
        switch (this.__loadStatus) {
            case loadingModes.Loading:
            case loadingModes.LoadIn:
                this.__loadStatus = loadingModes.LoadOut;
                this.__loaderOpacity = 1;
                this.__loader.style.visibility = "visible";
                this.__loadOutPromise = this.__stopLoadLoop();
                await this.__loadOutPromise;
                break;
            case loadingModes.NotLoading:
                break;
            case loadingModes.LoadOut:
                await this.__loadOutPromise;
                break;
        }
        return true;
    }

    async __stopLoadLoop() {
        while((this.__loaderOpacity > 0) && (this.__loadStatus == loadingModes.LoadOut)){
            this.__loaderOpacity -= 0.01;
            this.__loader.style.opacity = this.__loaderOpacity;
            await sleep(10);
        }
        this.__loadStatus == loadingModes.NotLoading;
        this.__loader.style.visibility = "hidden";
        this.__loaderOpacity = 1;
        this.__loader.style.opacity = this.__loaderOpacity;

    }
}
